To run, run `run.bat` with "Run as administrator".
If you still get an error about permissions, right click `version` > Properties and uncheck "Read-only".

You will also need to install [NSIS](https://nsis.sourceforge.io) and the nsProcess [plug-in](https://nsis.sourceforge.io/NsProcess_plugin) to build installers for WLM 2009 and Yahoo! Messenger.

`msi2xml` is from http://msi2xml.sourceforge.net.

`e_wise` is from https://kannegieser.net/veit/quelle/index_e.htm.

`escargot.dll` is built from https://gitlab.com/escargot-chat/msn-switcher.
